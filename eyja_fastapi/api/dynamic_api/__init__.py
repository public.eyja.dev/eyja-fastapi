from .model_api import model_api, set_auth_mode


ADMIN = {'all':'admin'}

__all__ = [
    'model_api', 'set_auth_mode',
    'ADMIN',
]
